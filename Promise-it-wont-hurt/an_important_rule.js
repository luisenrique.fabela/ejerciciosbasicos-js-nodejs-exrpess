'use strict';

function iterate(num) {
    console.log(num);
    return num + 1;
}

function alwaysThrows() {
    throw new Error('OH NOES');
}

function reject(error) {
    console.log(error.message);
}

Promise.resolve(iterate(1))
    .then(iterate)
    .then(iterate)
    .then(iterate)
    .then(iterate)
    .then(alwaysThrows)
    .then(iterate)
    .then(iterate)
    .then(iterate)
    .then(iterate)
    .then(iterate)
    .catch(reject);