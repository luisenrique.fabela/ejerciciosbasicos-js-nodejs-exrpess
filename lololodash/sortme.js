var _ = require("lodash");



var characters = [
    { article: 41,   quantity: 24 },
    { article: 2323, quantity: 2  },
    { article: 655,  quantity: 23 }
]

var worker = function(characters) {
     return _.sortBy(characters, elem => -elem.quantity);
};

module.exports = worker;