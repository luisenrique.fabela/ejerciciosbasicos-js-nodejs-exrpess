var _ = require("lodash");

var worker = function(poblaciones) {
      return _.forEach(poblaciones, function(elem) {
            if (elem.population > 1) {
              elem.size = "big";
            } else if(elem.population > 0.5) {
              elem.size = "med";
            } else {
              elem.size = "small";
            }
      });
};


module.exports = worker;