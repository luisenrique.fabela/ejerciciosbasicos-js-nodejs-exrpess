var _ = require("lodash");

var worker = function(towns) {
  var col = {
    hot: [],
    warm: []
  };

  var isHot, isWarm;
  _.forEach(towns, function (val, key) {
    isHot = _.every(val, function (n) {
      return n > 19;
    });
    isWarm = _.some(val, function (n) {
      return n > 19;
    });
    if (isHot) {
      col.hot.push(key);
    }
    if (!isHot && isWarm) {
      col.warm.push(key);
    }
  });
  return col;
};

// export the worker function as a nodejs module
module.exports = worker;



/*
const _ = require("lodash");

    var tempsort = function (item) {

        var result = {
            hot: [],
            warm: []
        };

        // If temp > 19
        const check_temp = (item) => item > 19;

        _.forEach(item, function (town, townname) {

            if (_.every(town, check_temp)) {
                result.hot.push(townname);
            } else if (_.some(town, check_temp)) {
                result.warm.push(townname);
            }

        });

        return result;
    };

    module.exports = tempsort;
*/