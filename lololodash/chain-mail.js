var _ = require('lodash');

var lista= ['Test', 'Hello', 'World', 'Node', 'JavaScript']
var worker = function(lista) {
    return _.chain(lista)
        .map(function (argumento) {
        return argumento + 'chained';
    })
        .map(function (argumento) {
        return argumento.toUpperCase();
    }).sortBy();
};
  
module.exports = worker;
