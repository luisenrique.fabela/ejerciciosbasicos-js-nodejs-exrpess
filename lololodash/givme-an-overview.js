var _ = require("lodash");

var orders =[ 
    { article: 1, quantity: 4 },
    { article: 2, quantity: 2 },
    { article: 1, quantity: 5 } 
]


var worker = function(orders) {
    var grouped = _.groupBy(orders, function (order) {
        return order.article;
    });
    var acc = [];
    _.forEach(grouped, function (allOrders, name) {
        acc.push({article: parseInt(name), total_orders: _.reduce(allOrders, function (sum, order) {
            return sum + order.quantity;
        }, 0)});
    });
    return _.sortBy(acc, 'total_orders').reverse();
};

// export the worker function as a nodejs module
module.exports = worker;



_.reduce(
    [1, 2, 3],
    function(sum, num) {
        return sum + num;
    }
)

    var worker = function(/* arguments */) {
        // do work; return stuff
    };

    // export the worker function as a nodejs module
    module.exports = worker;